(ns environments.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.algo.monads :as monads]
            [instaparse.core :as insta]
            [clojure.core.typed :as typed])
  (:use midje.sweet)
  (:gen-class))

;;; # Environments
;;;

;;; ## Basic environment representation
;;;
;;; The basic operations of an environment are updating and lookup
(defprotocol PEnviron
  (update [this ident value])
  (lookup [this ident]))

(defrecord environ [current]
  PEnviron
  (update [this ident value]
    (environ. (assoc current ident value)))

  (lookup [this ident]
    (current ident)))

(facts "basic environ tests"
  (let [e    (environ. {})
        e2   (update e :hi 3)]
    (lookup e2 :hi) => 3
    (lookup e2 :ho) => nil))

;;; ## A Basic environment with memory

(defprotocol PHistory
  (getHist [this]))

(defrecord histEnviron [history current]
  PEnviron
  (update [this ident value]
    (histEnviron. (conj history (environ. current))
                  (assoc current ident value)))

  (lookup [this ident]
    (current ident))

  PHistory
  (getHist [this]
    history))

(facts "basic environ with memory tests"
  (let [e   (histEnviron. [] {})
        e2  (update e :hi 3)
        e3  (update e2 :hi 2)]
    (lookup e3 :hi) => 2
    (lookup e2 :hi) => 3
    (lookup (second (getHist e3)) :hi) => 3))

;;; ## To use the above, a little program to evaluate additions
;;;
;;; Here we write a simple program that can evaluate expressions such
;;; as
;;;
;;;   (+ 2 2)
;;;   (+ 2 var1)
;;;   (+ (+ 2 var1) var2)
;;;
;;; in different environments.  Here the environment provides for the
;;; values of the variables.

(defn eval-expression [expression environment]
  (cond
   (number? expression)
   expression

   (symbol? expression)
   (if-let [val   (lookup environment expression)]
     val
     (throw (Exception. "symbol not in environemtn")))

   (= (first expression) '+)
   (+ (eval-expression (second expression) environment)
      (eval-expression (second (rest expression)) environment))))

(facts
  (eval-expression '3 (environ. {})) => 3
  (eval-expression '(+ 2 2) (environ. {})) => 4
  (eval-expression '(+ 2 hi) (environ. {'hi 2})) => 4)
